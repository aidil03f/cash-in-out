<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserBalanceHistory;

class UserBalanceHistoryController extends Controller
{

    public function index()
    {
        $user = auth()->user()->id;
        $data = UserBalanceHistory::get()->where('userBalanceId',$user);
        return response()->json($data);
    }
   
}
