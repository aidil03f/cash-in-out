<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class UserController extends Controller
{
    public function index()
    {
        return User::get();
    }

    public function show($id)
    {
        return User::find($id);
    }

    public function update($id)
    {
        request()->validate([
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email'
        ]);

        $user = User::findOrFail($id);
        $user->update([
            'name'     => request('name'),
            'username' =>  request('username'),
            'email'    =>  request('email'),
            'password' =>  bcrypt(request('password')),
        ]);
        
        return response()->json(['status' => 200]);
    }
}