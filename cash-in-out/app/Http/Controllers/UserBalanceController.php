<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{UserBalance,UserBalanceHistory};
use Stevebauman\Location\Facades\Location;

class UserBalanceController extends Controller
{
    public function index()
    {
        $user = auth()->user()->id;
        $get = UserBalance::get()->where('userId',  $user);
        $balanceAchieve = UserBalance::where('userId', $user)->get('balanceAchieve')->last();
        return response()->json(compact('get','balanceAchieve'));
      
    }


    public function store()
    {
        
        request()->validate([
            'balance' => 'required|numeric',
        ]);
        
        $user = auth()->user()->id;

        $get = UserBalance::where('userId', $user)->get('balance')->sum('balance');

        $balance = request('balance') + $get ?? 0;
        
        UserBalance::create([
            'userId' =>  $user,
            'balance' =>  request('balance'),
            'balanceAchieve' =>  $balance,
        ]);
         
         $history = new UserBalanceHistory;
         $history->userBalanceId = $user;
         $history->balanceBefore = $get ?? 0;
         $history->balanceAfter = $balance;
         $history->activity = 'add balance';
         $history->type = 'debit';
         $history->ip = request()->ip();
         $history->location = request('location') ?? 'Jakarta';
         $history->userAgent = 'Apps';
         $history->author = auth()->user()->name;
         $history->save();

        return response()->json(['status' => 200]);
    }

    public function location()
    {
        $location = Location::get('https://ip-api.com/');

        return $data = $location->regionName;
    }
    
}
