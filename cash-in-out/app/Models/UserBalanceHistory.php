<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBalanceHistory extends Model
{
    use HasFactory;

    protected $fillable = [
        'userBalanceId','balanceBefore','balanceAfter','activity','type','ip','location','userAgent','author'
    ];

    
}
