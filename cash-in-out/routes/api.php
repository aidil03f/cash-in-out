<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\{LoginController,RegisterController,LogoutController};
use App\Http\Controllers\{UserBalanceHistoryController,UserBalanceController,UserController};
use Illuminate\Support\Facades\Route;



Route::middleware('auth:api')->group(function(){
    
    Route::get('/userbalance',[UserBalanceController::class,'index']);
    Route::post('/userbalance/create',[UserBalanceController::class,'store']);
    Route::get('/userbalance/history',[UserBalanceHistoryController::class,'index']);
    
    Route::get('/user',[UserController::class,'index']);
    Route::get('/user/{id}/show',[UserController::class,'show']);
    Route::post('/user/create',RegisterController::class);
    Route::put('/user/{id}/edit',[UserController::class,'update']);
});

Route::get('/userbalance/location',[UserBalanceController::class,'location']);

Route::post('login',LoginController::class);
Route::post('logout',LogoutController::class);

