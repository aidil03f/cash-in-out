<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserBalanceHistory;

class UserBalanceHistorySeeder extends Seeder
{

    public function run()
    {
        UserBalanceHistory::create([
            'userBalanceId' => '1',
            'balanceBefore' => '100',
            'balanceAfter' => '200',
            'activity' => 'add balance',
            'type' => 'debit',
            'ip' => '127.0.0.1',
            'location' => 'Jakarta',
            'userAgent' => 'Apps',
            'author' => 'Jhon Doe'
        ]);
    }
}
