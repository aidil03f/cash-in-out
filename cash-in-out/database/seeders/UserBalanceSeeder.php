<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserBalance;

class UserBalanceSeeder extends Seeder
{
    public function run()
    {
        UserBalance::create([
            'userId' => '1',
            'balance' => '100',
            'balanceAchieve' => '100',
        ]);
    }
}
