import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import UserBalance from "../views/UserBalance.vue";
import UserBalanceCreate from "../views/UserBalanceCreate.vue";
import UserBalanceHistory from "../views/UserBalanceHistory.vue";
import BankBalance from "../views/BankBalance.vue";
import BankBalanceHistory from "../views/BankBalanceHistory.vue";
import UserIndex from "../views/user/Index.vue";
import UserShow from "../views/user/Show.vue";
import UserCreate from "../views/user/Create.vue";
import UserEdit from "../views/user/Edit.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "login.index",
    component: Login,
  },
  {
    path: "/user",
    name: "user.index",
    component: UserIndex,
  },
  {
    path: "/user/:id/show",
    name: "user.show",
    component: UserShow,
  },
  {
    path: "/user/:id/edit",
    name: "user.edit",
    component: UserEdit,
  },
  {
    path: "/user/create",
    name: "user.create",
    component: UserCreate,
  },
  {
    path: "/userbalance",
    name: "userbalance.index",
    component: UserBalance,
  },
  {
    path: "/userbalance/create",
    name: "userbalance.create",
    component: UserBalanceCreate,
  },
  {
    path: "/userbalance/history",
    name: "userbalancehistory.index",
    component: UserBalanceHistory,
  },
  {
    path: "/bankbalance",
    name: "bankbalance.index",
    component: BankBalance,
  },
  {
    path: "/bankbalance/history",
    name: "bankBalancehistory.index",
    component: BankBalanceHistory,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  linkActiveClass: "active",
});

export default router;
